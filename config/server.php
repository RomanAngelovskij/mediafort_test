<?
define('DB_DATABASE', 'tests');
define('DB_USER',     'tests');
define('DB_PASSWORD', '');
define('DB_HOST',     'localhost');

define('SITE_NAME',				'TestZadanie');
define('HOST_NAME',				'test7.r869.ru');
define('HOST_NAME_SHORT',	'test7.r869.ru');
define('COOKIE_DOMAIN',	  '.test7.r869.ru');

define('HOST_TYPE',     	'local');
define('CACHE_PREFIX',    'tz_');

session_set_cookie_params(3600, '/', HOST_NAME_SHORT);

ini_set('magic_quotes_gpc', '0');
ini_set('display_errors', 'On');
error_reporting (E_ALL ^ E_NOTICE);
setlocale(LC_CTYPE, "ru_RU.CP1251");
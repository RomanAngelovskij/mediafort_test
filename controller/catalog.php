<?
class Controller_Catalog extends Controller
{
	private $__view;

	private $__model;

	public function __construct(){
		$this->__view = new View();
		$this->__model = new Model_Catalog();
	}

	public function onIndex()
	{
		$models = $this->__model->find('createdAt DESC');
		$sites = [];
		foreach ($models as $model){
			$sites[] = $model->values;
		}
		$this->__view->add('sites', $sites);

		$this->__view->template = 'catalog/index.tpl';
		if (Request::isAjax() === true){
			$this->__view->template = 'catalog/_list.tpl';
		}

		return $this->__view->render();
	}

	public function onAdd(){
		$this->__view->template = 'catalog/_form.tpl';
		return $this->__view->render();
	}

	public function onSave(){
		$response = ['result' => true, 'errors' => false];

		$postData = Request::postVars(['name', 'url', 'logo', 'description', 'mail']);
		foreach ($postData as &$value){
			$value = iconv('UTF8', 'CP1251', strip_tags($value));
		}

		$this->__model->addFromHash($postData);

		$validateResult = $this->__model->validate();

		if ($validateResult !== true){
			foreach ($validateResult as $name => $msg){
				$validateResult[$name] = iconv('CP1251', 'UTF8', $msg);
			}

			$response['result'] = false;
			$response['errors'] = $validateResult;
		} else {
			$this->__model->createdAt = time();
			$this->__model->save();
		}

		exit(json_encode($response));
	}

	public function onView(){
		$id = $this->params[1];
		$site = $this->__model->findBy('id', $id);

		if (empty($site)){
			throw new ExceptionNotFound('���� � �������� �� ������');
		}

		$this->__view->add('site', $site[0]->values);
		$this->__view->template = 'catalog/view.tpl';

		return $this->__view->render();
	}


}
<?
class Controller_Error extends Controller
{
	static function notAvailable()
	{
		header('HTTP/1.0 503 Service Unavailable');
		$view = new View();
		$view->add('pagetitle',"������ �������� ����������");
		$view->template = 'errors/not-available.tpl';
		ob_clean();
		echo $view->render();
		exit;
	}
	
	
	static function forbidden($title='������ ��������',$comment='')
	{
		header("HTTP/1.0 403 Forbidden");
		$view = new View();
		$view->add('comment',$comment);
		$view->add('pagetitle',$title);
		$view->template = 'errors/forbidden.tpl';
		ob_clean();
		echo $view->render();
		exit;
	}
	
	static function notFound($title='�������� �� �������',$comment='')
	{
		$uri = substr($_SERVER['REQUEST_URI'],1);
		if(is_int(strpos($uri,'?'))) 
		$uri=substr($uri,0,strpos($uri,'?'));
		$urii = strtolower($uri);
		
		header("HTTP/1.0 404 Not Found");
		$view = new View();
		$view->add('url',$urii);
		$view->add('pagetitle',$title);
    $view->add('comment',$comment);
		$view->template = 'errors/404.tpl';
		ob_clean();
		echo $view->render();
		exit;
	}
	
	
	static function internalError($title='���������� ������',$comment='')
	{
		$uri = substr($_SERVER['REQUEST_URI'],1);
		if(is_int(strpos($uri,'?'))) 
		$uri=substr($uri,0,strpos($uri,'?'));
		$urii = strtolower($uri);
		
		header("HTTP/1.0 500 Internal Server Error");
		$view = new View();
		$view->add('url',$urii);
		$view->add('pagetitle',$title);
    $view->add('comment',$comment);
		$view->template = 'errors/internal.tpl';
		ob_clean();
		echo $view->render();
		exit;
	}
}
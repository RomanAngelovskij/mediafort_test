<?
class Controller_Subscribe extends Controller
{
	public function onSubscribe($hash = Array(), $errors=Array())
	{
		$view = new View();
		$view->add("hash", $hash);
		$view->add("errors", $errors);
		$view->add("pagetitle", '�������� �����');
		$view->template = 'subscribe/subscribe.tpl';
		return $view->render();
	}
	
	
	public function onSubmit()
	{
		$hash = Request::postVars( array('email') );
		
		$validator = new Validator();
		$validator->add( new Rule_NotEmpty	('email','������� ����� e-mail ��� ������') );
		$validator->add( new Rule_Email			('email') );
		$success = $validator->validate($hash);
		if(!$success)
		{
			return $this->onSubscribe($hash, $validator->errors);
		}
		
		$obj = new Model_Subscription();
		$obj->addFromHash($hash);
		$obj->dt = time();
		$obj->save();
		
		Redirect('/subscribe/done/');
	}
	
	
	public function onDone()
	{
		$view = new View();
		$view->add("pagetitle", '����� �������� � ����!');
		$view->template = 'subscribe/subscribe-done.tpl';
		return $view->render();
	}

}
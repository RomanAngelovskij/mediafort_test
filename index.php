<?
define('BASE_DIR', dirname(__FILE__));

// ������ ������� � �������� �������� ����
include BASE_DIR.'/lib/autoload.php';
include BASE_DIR.'/lib/utils.php';
include BASE_DIR.'/config/server.php';
include BASE_DIR.'/lib/db.php';
include BASE_DIR.'/lib/controller.php';
include BASE_DIR.'/lib/view.php';
include BASE_DIR.'/lib/exceptions.php';
include BASE_DIR.'/lib/smarty3/Smarty.class.php';

// ��������� ������������� ����������
include BASE_DIR.'/service/startup.php';
spl_autoload_register('my_autoloader');

try
{
	// ������ ������ URL-�������
	$CONFIG = parse_ini_file("config/application.ini", true);

	Service_Startup::sendHeaders();
	Service_Startup::startSession();
	Service_Startup::connectDatabase();

	list($controllerClass,$event,$params) = Service_Startup::parseUrl();

	if(!$controllerClass) throw new ExceptionNotFound();
	$controller = new $controllerClass();
	$response = $controller->run($event,$params);
}
catch(ExceptionNotFound $ex)
{
	$response = Controller_Error::notFound( $ex->title, $ex->comment );
}
catch(ExceptionForbidden $ex)
{
	$response = Controller_Error::forbidden( $ex->title, $ex->comment );
}
catch(ExceptionNotAvailable $ex)
{
	$response = Controller_Error::notAvailable();
}
catch(Exception $ex)
{
	$response = Controller_Error::internalError('���������� ������', $ex->getMessage() );
}

mysql_close();

echo $response;

exit;
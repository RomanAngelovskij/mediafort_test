$(document).ready(function(){
    $(document).on('click', 'a.modal', function(e){
        e.preventDefault();

        var url = $(this).data('href');

        $.ajax({
            method: 'get',
            type: 'html',
            url: url,
            data: {},
            cache: false,
            success: function(data){
                var modal =  $('<div>').addClass('modal').html(data);
                var overleay = $('<div>').addClass('modal-overlay').append(modal);

                $('body').append(overleay);
            }
        });
    })
})

$(document).on('submit', '.form-ajax', function(){
    clearErrors();

    var formElements = $(this).find(':input, textarea');
    var isValid = true;

    formElements.each(function(indx, elem){
        var rules = $(elem).data('rules');

        if (typeof rules !== 'undefined'){
            var result = validate(elem, rules)
            if (result !== true){
                isValid = false;
                var errorWrap = $(this).next('.form-errors');
                var errorMsg = '';

                if (errorWrap.length > 0){
                    $.each(result, function(indx, err){
                        errorMsg += err.msg + '<br>';
                    })
                    $(errorWrap).html(errorMsg);
                }
            }
        }
    })

    var formData = $(this).serialize();

    if (isValid === true){
        var url = $(this).attr('action');
        $.post(url, formData, function(response){
            console.log(response);
            if (response.result === false){
                $.each(response.errors, function(name, value){
                    var errorWrap = $('input[name=' + name + '], textarea[name=' + name + ']').next('.form-errors');
                    $(errorWrap).html(value);
                })
            } else {
                console.log(response);
                $('#list').load('/catalog/');
                $('.modal-overlay').remove();
            }
        }, 'json');
    }

    return false;
})

function clearErrors(){
    $('.form-errors').html('');
}

function validate(element, rules){
    var rules = rules.split(',');

    var isValid = true;

    var errors = {};
    errors = {};

    $.each(rules, function(indx, value){
        value = $.trim(value);
        var rule = value.split(':');
        var ruleName = rule[0];

        try{
            var result = validationRules[ruleName]($(element).val(), rule[1]);

            if (result.state === 'false'){
                errors[ruleName] = result;
                isValid = false;
            }
        } catch (err) {
            console.error(ruleName + ': ' + err);
        }
    })

    return isValid === true ? true : errors;
}

var validationRules = {
    'required': function(data){
        var result = {state: 'true', msg: ''};

        if (typeof data === 'undefined' || data.length === 0){
            result.state = 'false';
            result.msg = 'Обязательное поле';
        }

        return result;
    },
    'min': function(data, value){
        var result = {state: 'true', msg: ''};

        if (data.length < value){
            result.state = 'false';
            result.msg = 'Минимальная длина - ' + value;
        }
        return result;
    },
    'max': function(data, value){
        var result = {state: 'true', msg: ''};

        if (data.length > value){
            result.state = 'false';
            result.msg = 'Максимальная длина - ' + value;
        }

        return result;
    },
    'url': function(data){
        var result = {state: 'true', msg: ''};

        var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        if(!regexp.test(data)) {
            result.state = 'false';
            result.msg = 'Неправильный формат URL';
        }

        return result;
    },
    'email': function(data){
        var result = {state: 'true', msg: ''};

        var regexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!regexp.test(data)) {
            result.state = 'false';
            result.msg = 'Неправильный формат e-mail';
        }

        return result;
    }
}
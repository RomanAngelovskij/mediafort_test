<?
function my_autoloader($class_name) 
{
	$class_name = strtolower($class_name);
	$paths = explode('_', $class_name);
	if(sizeof($paths)>1)
	{
		if($paths[0]=="controller" || $paths[0]=="model" || $paths[0]=="service")
		{
			if(file_exists(BASE_DIR.'/'.$paths[0].'/'.$paths[1].'.php'))
			{
				include(BASE_DIR.'/'.$paths[0].'/'.$paths[1].'.php');
			}
		}
		else
		{
			if(file_exists(BASE_DIR.'/lib/'.$paths[0].'/'.$paths[1].'.php'))
			{
				include(BASE_DIR.'/lib/'.$paths[0].'/'.$paths[1].'.php');
			}
		}
	}
	else
	{
		include(BASE_DIR.'/lib/'.$paths[0].'.php');
	}
}
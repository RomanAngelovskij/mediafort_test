<?
class Controller
{
	public $params;
	
	public function run($handler,$params)
	{
		$this->params = $params;
		
		return $this->$handler();
	}
	
}
<?
/*
����� ��� ������ � ����� ������.

�������������:

DB::query()        - ��������� ������������ sql-������
DB::scalarSelect() - ���������� ��������� ������� � ���� ����� ���������� (��������: select id from ...)
DB::singleRow()    - ���������� ���� ������ � ���� �������������� ������� (��������: select * from users where id=1)
DB::getRows()      - ������������ ������ ����� (��������: select * from users)
DB::hashedSelect() - ���������� ������������� ������, � ������� ����� - ��� ������ ������� ������, � �������� ������ ������� (��������: select id,title from users)
DB::hashedRows()   - ������������ ������������� ������, � ������� ����� - ��� ������ ������� ������, � �������� - ��� ������ ���� �������� ������� ������ (��������: select id,title,rating from users)

��� ����������������� ������� ������� "������������".
��������:

DB::scalarSelect("select * from news where id=?i and title=?", 10, "Hello World");

������ �������������:
? - ��� �����
?i - ��� integer
?f - ��� float

����� ���� ������� DB::prepare() - ������� ���� ������������ ������������, �� ������ �� ���������, � ���������� ��� ���������:
$sql = DB::prepare("insert into news (title) values (?)", $title);

��������� �������:

DB::getAffectedRows() - ���������� ���������� ��������������� �����
DB::getInsertId() - ���������� ������������� �� ���������� �������

����� ��� ���� ��������������� ����� BulkInsert ��� �������, ����� ���� �������� ����� ����� ����� �� ���� ���.
�������� �� ����� �����:
$bi = new BulkInsert("insert into news (title)");
for($i=1; $i<=10000; $i++)
{
	$bi->add("?", "news number {$i}"); // ��� ���� �������� ������������
}
$bi->commit();

������ ����������� �� ���� ����������� ���������� insert-�������� � ���� ������� ������. 
�������� �� ������� �������, ��� ������ ��������� insert.

*/
class DB
{
	static $affected_rows;
	static $insert_id;
	
	static function connect($host, $user, $password, $database, $collation='CP1251')
	{
		$res = mysql_connect($host, $user, $password);
		if($res) $res = mysql_select_db($database);
		if(!$res) 
		{
			self::logConnectionError(mysql_error());
			throw new ExceptionNotAvailable();
		}
		mysql_query("SET NAMES ".$collation);
	}
	
	
	static function query($sql)
	{
		if(func_num_args()>1)	$sql = self::prepare($args = func_get_args());
		
		$st = microtime(true);
		$result = mysql_query($sql);
		if(mysql_error()) DB::error($sql);
		self::$affected_rows = mysql_affected_rows();
		self::$insert_id = mysql_insert_id();
		

		if(microtime(true)-$st>0.1 && $_SERVER['REMOTE_ADDR']=='93.125.42.170') $GLOBALS['queries'][] = Array("time"=>(microtime(true)-$st), "sql"=>$sql);
		
		return $result;
	}
	
	
	
	static function getCursor($sql)
	{
		if(func_num_args()>1)	$sql = self::prepare($args = func_get_args());
		
		return mysql_query($sql, self::$link);
	}
	
	
	static function fetch($cursor)
	{
		$row = mysql_fetch_assoc($cursor);
		if(!$row) return false;
		
		if(count($row)==1) return current($row);
		return $row;
	}
	
	
	static function getAffectedRows()
	{
		return self::$affected_rows;
	}
	
	
	static function getInsertId()
	{
		return self::$insert_id;
	}
	
	
	static function delete($table, $condition)
	{
		DB::query("DELETE FROM $table WHERE $condition");
	}
	
	
	static function clearTableCache($table)
	{
		// deprecated
	}
	
	
	static function singleRow($sql)
	{
		if(func_num_args()>1)	$sql = self::prepare($args = func_get_args());
		
		$res = DB::query($sql);
		if($row = mysql_fetch_assoc($res))
		{
			return $row;
		}
		return false;
	}
	
	
	static function scalarSelect($sql)
	{
		if(func_num_args()>1)	$sql = self::prepare($args = func_get_args());
		
		$res = DB::query($sql);
		if($row = mysql_fetch_row($res))
		{
			return $row[0];
		}
		return false;
	}
	
	
	static function getRows($sql)
	{
		if(func_num_args()>1)	$sql = self::prepare($args = func_get_args());
		
		$ret=Array();
		$res = DB::query($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$ret[] = $row;
		}
		mysql_free_result($res);
		return $ret;
	}
	
	
	static function hashedSelect($sql)
	{
		if(func_num_args()>1)	$sql = self::prepare($args = func_get_args());
		
		$ret=Array();
		$res = DB::query($sql);
		while($row = mysql_fetch_row($res))
		{
			$ret[$row[0]] = $row[1];
		}
		mysql_free_result($res);
		return $ret;
	}
	
	
	static function hashedRows($sql)
	{
		if(func_num_args()>1)	$sql = self::prepare($args = func_get_args());
		
		$ret=Array();
		$res = DB::query($sql);
		while($row = mysql_fetch_assoc($res))
		{
			$first = current($row);
			$ret[$first] = $row;
		}
		mysql_free_result($res);
		return $ret;
	}
	
	function getArray($sql)
	{
		if(func_num_args()>1)	$sql = self::prepare($args = func_get_args());
		
		$ret=Array();
		$res = DB::query($sql);
		while($row = mysql_fetch_array($res))
		{
			$ret[] = $row[0];
		}
		mysql_free_result($res);
		return $ret;
	}
	
	
	public static function insert($table, $values=Array())
	{
		$fields=array_keys($values);
		$fields_str = implode(',', $fields);
		$values_str ="'".implode("', '", self::escape($values) )."'";
		
		$sql = "insert into $table ($fields_str) values ($values_str)";
		DB::query($sql);

		return self::getInsertId();
	}
	
	
	function update($table, $id, $values=Array())
	{
		$id = intval($id);
		unset($values['id']);
		
		$fields=array_keys($values);
		$fields_str ='';
		foreach($fields as $field)
		{
				if($fields_str) $fields_str.=',';
				$fields_str.=$field."='".self::escape($values[$field])."'";
		}
		
		$sql = "update $table set $fields_str where id={$id}";
		DB::query($sql);
		
		return self::getAffectedRows();
	}
	
	
	
	static function escape($value)
	{
		if(is_array($value))
		{
			return array_map("mysql_real_escape_string", $value);
		}
		else
		{
			return mysql_real_escape_string($value);
		}
	}
	
	
	// usage: prepare($sql, $args)
	//    or: prepare($args)
	function prepare()
	{
		if(func_num_args()==1) $args = func_get_arg(0); // ��� ���� ���� ��������� �������� ��������
		else $args = func_get_args();
		
		$sql = array_shift($args);
		$sql.=' ';
		
		$i=0;
		$shift=0;
		$pos = strpos($sql, '?', $shift);
		while(is_int($pos))
		{
			$pos2 = $pos+1;
			
			$key = '';
			$next_char = substr($sql, $pos+1, 1);
			if($next_char==='i' || $next_char==='l' || $next_char==='f') 
			{
				$key = $next_char;
				$pos2++;
			}
			
			if($key=='i')
			{
				$subst = intval($args[$i]);
			}
			elseif($key=='f')
			{
				$subst = floatval($args[$i]);
			}
			elseif($key=='l')
			{
				$subst = "'%".mysql_real_escape_string(str_replace('%','',$args[$i]))."%'";
			}
			else
			{
				$subst = "'".mysql_real_escape_string($args[$i])."'";
			}
			
			//  ��������� �����������
			$sql = substr($sql,0,$pos) . $subst . substr($sql,$pos2);
			
			// ���� ��������� placeholder
			$i++;
			$shift = $pos + strlen($subst) + 1;
			$pos = strpos($sql, '?', $shift);
		}
		
		return $sql;
	}
	
	
	function logConnectionError($error='')
	{
		date_default_timezone_set("Europe/Moscow");
		
		$str = date("H:i:s")." ".$error."\n";
		
		$fp = fopen(BASE_DIR.'/tmp/mysql.'.date("d.m.Y").'.log','a');
		fwrite($fp,$str);
		fclose($fp);
	}
	
	
	function error($sql)
	{
		$info = mysql_error();
		
		if( is_int(strpos($info,'Lost connection')) || is_int(strpos($info,'of memory')) || is_int(strpos($info,'has gone')) )
		{
			self::logConnectionError();
			throw new ExceptionNotAvailable();
		}
		
		$str = "������ ��� ���������� �������: ".$sql."\r\n<br>";
		$str .= "�������� ������: ".$info."\r\n";
		$str .= "����� �� �������� ��������� ������: ".$_SERVER['REQUEST_URI']."\r\n";
		$str .= "\r\n";
		
		date_default_timezone_set("Europe/Moscow");
		$fp = fopen(BASE_DIR.'/tmp/error.'.date("d.m.Y").'.log','a');
		fwrite($fp,$str);
		fclose($fp);
		
		//debug_print_backtrace();			exit;
		
		throw new ExceptionDB($sql, $info);
	}
}


class ExceptionDB extends Exception
{
	public $sql;
	public $info;
	
	public function __construct($sql, $info)
	{
		$this->sql = $sql;
		$this->info = $info;
		parent::__construct( "������ ��� ���������� �������." );
  }
}


class BulkInsert
{
	public $pre;
	public $str;
	public $maxlen = 30000; // DB max_allowed_packet
	
	public function __construct($pre)
	{
		$this->pre = $pre;
		$this->str = '';
  }
	
	function add($sql)
	{
		if(func_num_args()>1)	$sql = DB::prepare($args = func_get_args());
		if($this->str!='') $this->str = $this->str.',';
		$this->str .= '('.$sql.')';
		if(strlen($this->str)>$this->maxlen) $this->commit();
	}
	
	function commit()
	{
		if($this->str!='')
		{
			DB::query($this->pre.' values '.$this->str);
			$this->str = '';
		}
	}
}
<?
class Request
{
	static function getVars($strings)
	{
		$list = array();
		foreach($strings as $name) 
		{
			$list[$name] = isset($_GET[$name]) ? self::escape($_GET[$name]) : null;
		}
		return $list;
	}
	
	static function postVars($variables) 
	{
		$list = array();
		foreach($variables as $name) 
		{
			$list[$name] = isset($_POST[$name]) ? self::escape($_POST[$name]) : null;
		}
		return $list;
	}
	
	static function vars($variables) 
	{
		$list = array();
		foreach($variables as $name) 
		{
			$list[$name] = isset($_REQUEST[$name]) ? self::escape($_REQUEST[$name]) : null;
		}
		return $list;
	}
	
	static function getInt($name)
	{
		if(isset($_GET[$name]))
			return intval($_GET[$name]);
		else
			return 0;
	}
	
	static function getStr($name)
	{
		if(isset($_GET[$name]))
			return self::escape($_GET[$name]);
		else
			return '';
	}
	
	static function postInt($name)
	{
		if(isset($_POST[$name]))
			return intval($_POST[$name]);
		else
			return 0;
	}
	
	static function postStr($name)
	{
		if(isset($_POST[$name]))
			return self::escape($_POST[$name]);
		else
			return '';
	}
	
	
	static function escape($str)
	{
		// ���� ������, �������� ��������� - �� �������
		if(is_array($str)) 
		{
			foreach($str as $k=>$v)
			{
				$str[$k] = htmlspecialchars( trim($v) );
			}
			return $str;
		}
		
		// 1. ����������� ����� Request:: ������
		// ��������� htmlspecialchars
		// The translations performed are: 
		// '&' (ampersand) becomes '&amp;' 
		// '"' (double quote) becomes '&quot;'
		// '<' (less than) becomes '&lt;'
	  // '>' (greater than) becomes '&gt;'
		
		return htmlspecialchars( trim($str) );
	}

	/**
	 * Detect if this Ajax request
	 * @return bool
	 */
	public static function isAjax(){
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
	}
}
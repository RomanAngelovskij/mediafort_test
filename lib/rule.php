<?
class Rule
{
	public $name;
	public $message;
	public $error_message="����������� ������ ������";
	public $isValidator=true;
	public $isUpdate;
	
	function __construct($name, $error_message="")
	{
		$this->name=$name;
		if($error_message!="") $this->error_message = $error_message;
	}
	
	function Validate($hash)
	{
		if($this->Check($hash)) return true;
		else 
		{
			$this->message = $this->error_message;
			return false;
		}
	}
}
<?
class Rule_Assert extends Rule
{
	public $val;
	public $error_message;
	
	function __construct($name, $val, $error_message="")
	{
		$this->name=$name;
		$this->val=$val;
		if($error_message!="") $this->error_message = $error_message;
	}
	
	function Check($hash)
	{
		if($this->val) return true;
		return false;
	}
}
<?
class Rule_Captcha extends Rule
{
	public $error_message="�� ����� �������� ����� � ��������";
	
	function Check($hash)
	{
		if(!$hash[$this->name]) 
		{
			$this->error_message = '�� ������ ������ ��� � ��������';
			return false;
		}
		
		if( !$_SESSION['captcha_keystring'] ) 
		{
			return false;
		}
		
		$user_input = $_SESSION['captcha_keystring'];
		unset($_SESSION['captcha_keystring']);
		
		if( $hash[$this->name] === $user_input ) 
		{
			return true;
		}
		
		return false;
	}
}

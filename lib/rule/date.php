<?
class Rule_Date extends Rule
{
	private $day;
	private $month;
	private $year;
	public  $error_message="������������ ������ ����";
	
	function __construct($name,$day,$month,$year, $error_message="")
	{
		$this->name=$name;
		$this->day=$day;
		$this->month=$month;
		$this->year=$year;
		if($error_message!="") $this->error_message = $error_message;
	}
	
	function Check($hash)
	{
		$day=intval($hash[$this->day]);
		$month=intval($hash[$this->month]);
		$year=intval($hash[$this->year]);
		if(!$day && !$month && !$year) return true;
		return checkdate($month,$day,$year);
	}
}
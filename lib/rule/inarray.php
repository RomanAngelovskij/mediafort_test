<?
class Rule_InArray extends Rule
{
	private $array;
	public $error_message="Недопустимое значение";
	
	function __construct($name, $array, $error_message='')
	{
		$this->name=$name;
		$this->array=$array;
		if($error_message!="") $this->error_message = $error_message;
	}
	
	function Check($hash)
	{
		if(in_array($hash[$this->name],$this->array)) return true;
		
		return false;
	}
}
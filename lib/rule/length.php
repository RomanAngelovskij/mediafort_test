<?
class Rule_Length extends Rule
{
	private $min;
	private $max;
	
	function __construct($name, $min, $max=-1)
	{
		$this->name=$name;
		$this->min=$min;
		$this->max=$max;
	}
	
	function Check($hash)
	{
		if(strlen($hash[$this->name])<$this->min) 
		{
			$this->error_message = "���� ��������� �� ����� ".$this->min." ".pluralForm($this->min,'�������','��������','��������');
			return false;
		}
		
		if($this->max!=-1 && strlen($hash[$this->name])>$this->max) 
		{
			$len = strlen($hash[$this->name]);
			$this->error_message = "���� ��������� �� ����� ".$this->max." ".pluralForm($this->max,'�������','��������','��������').'. ������: '.$len.'.';
			return false;
		}
		
		return true;
	}
}
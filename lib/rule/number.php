<?
class Rule_Number extends Rule
{
	private $min;
	private $max;
	private $allow_empty;
	public $error_message="��������� ������ ��������� ��������";
	
	function __construct($name, $error_message='',$min='',$max='',$allow_empty=true)
	{
		$this->name=$name;
		$this->min=$min;
		$this->max=$max;
		$this->allow_empty=$allow_empty;
		if($error_message!="") $this->error_message = $error_message;
	}
	
	function Check($hash)
	{
		if(strlen($hash[$this->name])==0)
		{
			if($this->allow_empty)
				return true;
			else
				return false;
		}
		
		if(!is_numeric($hash[$this->name])) return false;
		
		if($this->min && intval($hash[$this->name])<intval($this->min)) return false;
		if($this->max && intval($hash[$this->name])>intval($this->max)) return false;
		
		return true;
	}
}
<?
class Rule_OneOf extends Rule
{
	public $fields;
	public $error_message="��������� ���� �� ���� �� �����";
	
	function __construct($name,$fields=Array(), $error_message="")
	{
		$this->name=$name;
		$this->fields=$fields;
		if($error_message!="") $this->error_message = $error_message;
	}
	
	function Check($hash)
	{
		foreach($this->fields as $field)
		{
			if(isset($hash[$field]) && $hash[$field]) return true;
		}
		return false;
	}
}
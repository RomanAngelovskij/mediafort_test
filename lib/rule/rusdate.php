<?
class Rule_RusDate extends Rule
{
	public  $error_message="���� ������ ���� � ���� ��.��.����";
	
	function Check($hash)
	{
		if( !$hash[$this->name]) return true;
		
		$arr = explode('.',$hash[$this->name]);
		if(sizeof($arr)!=3) return false;
		
		$day=intval($arr[0]);
		$month=intval($arr[1]);
		$year=intval($arr[2]);
		
		return checkdate($month,$day,$year);
	}
}
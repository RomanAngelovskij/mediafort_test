<?php
function smarty_function_myblock($params, &$smarty)
{
	if(isset($params['handler']))
	{
		list($controllerClass,$event) = explode('.',$params['handler']);
		if($controllerClass=='Controller_Blocks')
		{
			return Controller_Blocks::$event($params);
		}
		else
		{
			return call_user_func($controllerClass . '::' . $event, $params);
		}
	}
	if(isset($params['type']))
	{
		$controller = new Controller_Blocks();
		return $controller->run("block",$params);
	}
}
?>

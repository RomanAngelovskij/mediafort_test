<?php
function smarty_function_random($params, &$smarty)
{
	$min = (isset($params['min']) ? intval($params['min']) : 100);
	$max = (isset($params['max']) ? intval($params['max']) : 10000);
	return rand($min,$max);
}
?>

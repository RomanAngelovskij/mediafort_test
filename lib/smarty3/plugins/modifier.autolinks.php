<?php
function smarty_modifier_autolinks($string)
{
	$sm_autolinks_preg = array(
    'pattern' => array(
        "'[\w\+]+://[A-z0-9\.\?\+\-/_=&%#:;]+[\w/=]+'si",
        "'([^/])(www\.[A-z0-9\.\?\+\-/_=&%#:;]+[\w/=]+)'si",
    ),
    'replacement' => array(
        '<a href="$0" target="_blank">$0</a>',
        '$1<a href="http://$2" target="_blank" class="newwindow">$2</a>',
    ),
	);
	$search = $sm_autolinks_preg['pattern'];
  $replace = $sm_autolinks_preg['replacement'];
	$string = preg_replace($search, $replace, $string);
  return $string;
}


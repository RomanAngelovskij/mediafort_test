<?php
function smarty_modifier_autologin($string)
{
	$cu = getCurrentUser();
	
	if(!$cu) return $string;
	
  return $cu->createAutologin($string);
}


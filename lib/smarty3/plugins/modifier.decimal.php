<?
function smarty_modifier_decimal($n, $delim=' ')
  {
		if($n<1000) return $n;
		
		$str = strrev($n);
		$arr = str_split($str,3);
		$ret = implode($delim, $arr);
		return strrev($ret);
  }
?>
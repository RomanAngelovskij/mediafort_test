<?php
function smarty_modifier_wrap($string, $max=13, $break="<br>", $forced=true)
{
	$string = str_replace('_',' ',$string);
	$string = trim($string);
	
	if(strlen($string) > $max)
	{
		$words = explode(' ',$string);
		if(sizeof($words)==2)
		{
			if(strlen($words[0])<$max && strlen($words[1])<$max)
			{
				return str_replace(' ','<br>',$string);
			}
		}
		$words = explode('-',$string);
		if(sizeof($words)==2)
		{
			if(strlen($words[0])<$max && strlen($words[1])<$max)
			{
				return str_replace('-','-<br>',$string);
			}
		}
	}
	
	$temp = str_replace('-',' ',$string);
	$words = explode(' ',$temp);
	foreach($words as $word)
	{
		if(strlen($word) > $max && strlen($word) < $max*2)
		{
			// ������� �������� ����� �������� ��� ��������...
			// ����� AlesechkaSolnechnaya
			$pos = 0;
			$middle = floor(strlen($word)/2);
			for($i = strlen($word) - $max;
					$i < $max;
					$i++)
			{
				$cur = substr($word,$i,1);
				$next = substr($word,$i+1,1);
				if($cur==strtolower($cur) && $next==strtoupper($next) && !is_numeric($cur)) 
				{
					$newpos = $i+1;
					// ����� ��� ����� � ��������� ����������
					if( abs($middle-$newpos) <  abs($middle-$pos) ) $pos = $newpos;
				}
			}
			
			if($pos>0)
			{
				$rep = substr($word,0,$pos).'<br>'.substr($word,$pos);
				$string = str_replace($word, $rep, $string);
			}
			else
			{
				$rep = wordwrap($word, $max, $break, true);
				$string = str_replace($word, $rep, $string);
			}
		}
		if(strlen($word) >= $max*2)
		{
			// ������� ������ - ���������� ��������� ��������
			$rep = wordwrap($word, $max, $break, true);
			$string = str_replace($word, $rep, $string);
		}
	}
	
	return $string;
}
?>

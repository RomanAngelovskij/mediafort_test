<?php
class Smarty_Internal_Compile_Split extends Smarty_Internal_CompileBase 
{
    public function compile($args, $compiler, $parameter)
    {
        // the following must be assigned at runtime because it will be overwritten in Smarty_Internal_Compile_Append
        $this->required_attributes = array('var', 'array','chunks');
        $this->shorttag_order = array('var', 'array','chunks');
        $this->optional_attributes = array();
        $_nocache = 'null';
        $_scope = Smarty::SCOPE_LOCAL;
        // check and get attributes
        $_attr = $this->getAttributes($compiler, $args);
        
				return "<?php\n \$_smarty_tpl->tpl_vars[".$_attr['var']."] = new Smarty_variable(Misc::array_split(".$_attr['array'].",".$_attr['chunks']."),".$_nocache.", ".$_scope."); ?>";
    }
}

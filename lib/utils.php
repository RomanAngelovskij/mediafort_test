<?
// ��������� �������, ������� ������������ ��� ������ ��� ����� ������ �������
function from1251($str)
{
	return iconv('Windows-1251','UTF-8',$str);
}

function to1251($str)
{
	return iconv('UTF-8','windows-1251//IGNORE//TRANSLIT',$str);
}


function rus2lat($str)
{
	$rus = array('�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�','�');
	$lat = array('e','zh','c','ch','sh','sh','ju','ja','E','ZH','C','CH','SH','SH','JU','JA','','','','');
	$str = str_replace($rus,$lat,$str);
	$str = strtr($str,
	    "����������������������������������������������",
	    "ABVGDEZIJKLMNOPRSTUFHIEabvgdezijklmnoprstufhie");
		return($str);
}

function urlMapping()
{
	global $CONFIG;
	
	$uri = substr($_SERVER['REQUEST_URI'],1);
	if(is_int(strpos($uri,'?'))) 
		$uri=substr($uri,0,strpos($uri,'?'));
	$urii = strtolower($uri);
	
	if(!$uri) return Array($CONFIG['url-mapper']['index'], Array());
	
	// ������ � ������� �� ����� ���� ���������� � ����� /.../.../
	foreach($CONFIG['shortcuts'] as $k=>$v)
	{
		if($urii==$k || $urii==$k.'/')
		{
			return Array($v,array());
		}
	}	
	
	// ��������� �� ���������:
	// site.com/classname/methodname/params...
	// � ������ ���� ������ 1 �������� - ���� ��� ��� ������� on{$Classname} �������� news = Controller_News.onNews
	$urii2 = $urii;  // strlower
	if($urii2 != '/' && substr($urii2,strlen($urii2)-1,1)=='/') $urii2 = substr($urii2,0,strlen($urii2)-1);
	$parts = explode('/',$urii2);
	if(count($parts)==1) $parts[1]=$parts[0]; // default action (on%classname%)
	if(count($parts)>=2 && preg_match("/^[a-z0-9]+$/",$parts[0]) && preg_match("/^[a-z0-9\-]+$/",$parts[1])) // only alphabet chars in classname
	{
		if(count($parts)>=2)
		{
			if($parts[0]=='ajax' && count($parts)>=3)
			{
				$_controller = 'Controller_'.ucfirst($parts[1]);
				$_method = 'ajax'.strtolower($parts[2]);
			}
			else
			{
				$_controller = 'Controller_'.ucfirst($parts[0]);
				$_method = 'on'.strtolower($parts[1]);
			}
			if( class_exists($_controller) && in_array($_method, array_map("strtolower",get_class_methods($_controller))) )
			{
				if($parts[0]=='ajax' && count($parts)>=3)
				{
					$uri = substr($uri, strlen($parts[0].'/'.$parts[1].'/'.$parts[2]) ); // ������� ��������� �����
				}
				else
				{
					$uri = substr($uri, strlen($parts[0].'/'.$parts[1]) ); // ������� ��������� �����
				}
				
				if($uri[strlen($uri)-1]=='/') $uri = substr($uri,0,strlen($uri)-1);
				$params = explode('/',$uri);
				
				return Array($_controller.'.'.$_method,$params);
			}
			
			if( class_exists($_controller) && method_exists($_controller,"router") )
			{
				$_method = call_user_func($_controller."::router", $parts);
				if($_method) 
				{
					if($uri[strlen($uri)-1]=='/') $uri = substr($uri,0,strlen($uri)-1);
					$params = explode('/',$uri);
					return Array($_controller.'.'.$_method,$params);
				}
			}
		}
	}
	
	
	
	$max=0;
	$match='';
	foreach($CONFIG['url-mapper'] as $k=>$v)
	{
		if(strpos($urii,$k)===0 && strlen($k)>$max && (strlen($k)==strlen($urii) || $urii[strlen($k)]=='?' || $urii[strlen($k)]=='/'))
		{
			$match = $v;
			$max = strlen($k);
		}
	}
	
	if($match)
	{
		$uri = substr($uri,$max);
		if($uri[strlen($uri)-1]=='/') $uri = substr($uri,0,strlen($uri)-1);
		$params = explode('/',$uri);
		
		return Array($match,$params);
	}
	
	
	return Array($CONFIG['url-mapper']['404'], Array($uri));
}



function queryBuilder($params=null,$prepare=false,$page="")
{
	if($page=="")
	{
		$page = substr($_SERVER["REQUEST_URI"],1);
		$page = str_replace("admin/","",$page);
	}
	if($params)	
	{
		$ps = explode("&", $params);
		if(!is_array($ps)) $ps=Array($ps);
		foreach($ps as $param){
			list($pname,$pval) = explode("=", $param);
			$page = str_replace("&".$pname."=".$_GET[$pname],"",$page);
			$page = str_replace("?".$pname."=".$_GET[$pname],"?",$page);
		}
		if( !is_int( strpos($page,"?") ) ) $page.="?";
		foreach($ps as $param){
			list($pname,$pval) = explode("=", $param);
			if($pval){
				$sep="";
				if( $page[strlen($page)-1]!="&" &&  $page[strlen($page)-1]!="?") $sep="&";
				$page.=$sep.$pname."=".$pval;
				}
			}
		}
	if($prepare)
	{
		if( is_int( strpos($page,"?") ) && $page[strlen($page)-1]!="?" && $page[strlen($page)-1]!="&") $page.="&";
		if( !is_int( strpos($page,"?")) ) $page.="?";
		}
	else
	{
		if( $page[strlen($page)-1]=="&" ) $page=substr($page,0,strlen($page)-1);
		if( $page[strlen($page)-1]=="?" ) $page=substr($page,0,strlen($page)-1);
	}
	$page=str_replace('?&','?',$page);
	
	if(strlen($page)>5 && substr($page,0,5)=='http:')
		return $page;
	else
		return 'http://'.$_SERVER['HTTP_HOST'].'/'.$page;
}


function Redirect($url)
{
	if(strlen($url)<7 || substr($url,0,7)!='http://') $url = 'http://'.HOST_NAME.$url;
	
	// �������� �� ��������
	if(!preg_match("/^http\:\/\/[a-z0-9\-]+\.".str_replace('.','\.',HOST_NAME_SHORT)."\//",$url) && !preg_match("/^http\:\/\/".str_replace('.','\.',HOST_NAME)."\//",$url))
	{
		$url = 'http://'.HOST_NAME.'/';
	}
	
	header("Location: $url");
	exit;
}


function ajax($str)
{
	echo $str;
	exit;
}


function instr($str, $substr)
{
	return is_int( strpos($str,$substr) );
}


function begin_session()
{
	if( !isset( $_SESSION ) ) 
	{
		my_start_session();
	}
}

function my_start_session()
{
	if(!isset($_COOKIE[session_name()]))
	{
		session_id( md5(rand(0, 999999).$_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'].microtime(false)) );
	}
	
	session_start();
	setcookie(session_name(), session_id(), time() + 10*86400, "/", COOKIE_DOMAIN);
}


function pluralForm($n, $form1, $form2, $form5)
  {
    $n = abs($n) % 100;
    $n1 = $n % 10;
    if ($n > 10 && $n < 20) return $form5;
    if ($n1 > 1 && $n1 < 5) return $form2;
    if ($n1 == 1) return $form1;
    return $form5;
  }


function rusdate2int($txt)
{
	if(!$txt) return 0;
	list($day,$month,$year) = explode('.',$txt);
	if(strlen($day)<2) $day='0'.$day;
	if(strlen($month)<2) $month='0'.$month;
	return strtotime($year.'-'.$month.'-'.$day);
}


function int2rusdate($dt)
{
	return date("d.m.Y",$dt);
} 




function checkdateRus($dt)
{
	$arr = explode('.',$dt);
	if(sizeof($arr)!=3) return false;
	
	$day=intval($arr[0]);
	$month=intval($arr[1]);
	$year=intval($arr[2]);
	
	return checkdate($month,$day,$year);
}




function getWeekDay($dt=0)
{
	if($dt==0) $dt=time();
	$wd = date("w",$dt);
	if($wd==0) return 7;
	else return $wd;
}



function implode2($arr, $default='0')
{
	$str = implode(',', $arr);
	if(!$str) $str = $default;
	return $str;
}

function array2json($i=array()) 
{
	$o='';
	foreach ($i as $k=>$v){
		if(is_array($v))
		{
			$o .= '"'.$k.'":'.array2json($v).',';
		}
		else
		{
			$v = str_replace("\\","\\\\",$v);
			$o .= '"'.$k.'":"'.str_replace('"','\"',$v).'",';
		}
	}
	$json = '({'.substr($o,0,-1).'})';
	$json = str_replace("\n",'\n',$json);
	$json = str_replace("\r",'',$json);
	return $json;
}





function safe_glob($pattern, $flags=0) 
{
    $split=explode('/',$pattern);
    $match=array_pop($split);
    $path=implode('/',$split);
    if (($dir=opendir($path))!==false) {
        $glob=array();
        while(($file=readdir($dir))!==false) {
            if (fnmatch($match,$file)) {
                if ((is_dir("$path/$file"))||(!($flags&GLOB_ONLYDIR))) {
                    if ($flags&GLOB_MARK) $file.='/';
                    $glob[]=$path.'/'.$file;
                }
            }
        }
        closedir($dir);
        if (!($flags&GLOB_NOSORT)) sort($glob);
        return $glob;
    } else {
        return false;
    }
}

if (!function_exists('fnmatch')) 
{
    function fnmatch($pattern, $string) 
		{
        return @preg_match('/^' . strtr(addcslashes($pattern, '\\.+^$(){}=!<>|'), array('*' => '.*', '?' => '.?')) . '$/i', $string);
    }
}



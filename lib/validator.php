<?
class Validator
{
	public $rules;
	public $errors;
	
	function __construct()
	{
		$this->rules=array();
		$this->errors=array();
	}
	
	function Validate($hash)
	{
		$success=true;
		foreach($this->rules as $rule)
		{
			if(!$this->errors[$rule->name]) // ��������� ���� ������ �� ������ ������ � ���
			{
				if(!$rule->validate($hash))
				{
					$success=false;
					$this->errors[$rule->name] = $rule->message;
				}
			}
		}
		return $success;
	}
	
	function add($obj)
	{
		$this->rules[]=$obj;
	}
}
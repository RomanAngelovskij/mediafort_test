<?
class View
{
	public $renderer;
	public $template;
	
	public function __construct() 
	{
		$this->renderer = new Smarty();
		$this->renderer->setTemplateDir(BASE_DIR.'/templates/');
		$this->renderer->setCompileDir(BASE_DIR.'/templates_c/');
		
		$this->renderer->assign("host_name", 'http://'.HOST_NAME);
		$this->renderer->assign("site_name", SITE_NAME);
	}
	
	public function render() 
	{
		return $this->renderer->fetch($this->template);
	}
	
	public function add($key,$value) {
		$this->renderer->assign($key,$value);
	}
}
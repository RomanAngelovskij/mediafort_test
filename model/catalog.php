<?php
class Model_Catalog extends Model
{
	public function __construct(){
		parent::__construct();
		$this->properties['catalog'] = ['id', 'url', 'logo', 'name', 'description', 'mail', 'createdAt'];
	}

	public function validate(){
		$hash = [];
		$validator = new Validator();

		foreach ($this->properties['catalog'] as $name){
			if ($name !== 'createdAt' && $name !== 'id'){
				$hash[$name] = $this->{$name};
				$validator->add(new Rule_NotEmpty($name, '������������ ����'));
			}
		}

		$validator->add(new Rule_Email('mail'));
		$validator->add(new Rule_Length('name', 5, 150));
		$validator->add(new Rule_Length('description', 50, 250));
		$validator->add(new Rule_Url('url'));
		$validator->add(new Rule_Url('logo'));

		$success = $validator->validate($hash);

		if ($this->__isUniqueURL($this->url) === false){
			$validator->errors['url'] = '����� URL ��� ���� � ��������';
			$success = false;
		}

		if ($success === false){
			return $validator->errors;
		}

		return true;
	}

	private function __isUniqueURL($url){
		$result = self::findByCondition("`url` = '" . DB::escape($url) . "'");

		return empty($result);
	}
}
<?
class Model_Subscription extends Model
{
	function __construct()
	{
		parent::__construct();
		$this->properties['subscription'] = array('id',
																							'dt',
																							'email');
	}
	
	
	function getDomain()
	{
		$domain = substr( $this->email, strpos($this->email,'@')+1 );
		return $domain;
	}
	
	
	function isGoodEmail()
	{
		$good_domains = array('mail.ru','yandex.ru','rambler.ru','gmail.com','bk.ru','list.ru','ukr.net','inbox.ru',
													'yahoo.com','ya.ru','hotmail.com','inbox.lv','bigmir.net','i.ua','yandex.ua','meta.ua',
													'pochta.ru','tut.by','tyt.by','gmx.de','ua.fm','sibmail.com','web.de','ngs.ru','gmail.ru','km.ru',
													'narod.ru','live.ru','yahoo.de','e1.ru','one.lv','pisem.net','qip.ru','online.ua','gala.net',
													'hotmail.de','hot.ee','nm.ru','land.ru','hotbox.ru','aol.com','freenet.de','seznam.cz',
													'voliacable.com','online.de','msn.com','yandex.by','hotmail.ru','front.ru','gmx.net','mail.ua',
													'li.ru','walla.com','walla.co.il','yahoo.co.uk','mail.com','spaces.ru','post.ru','live.com',
													'mail.by','lenta.ru','open.by','one.lt','aport.ru','yandex.com','yandex.kz','abv.bg','myrambler.ru',
													'ro.ru','googlemail.com');
		
		$domain = $this->getDomain();
		
		return in_array($domain, $good_domains);
	}
}
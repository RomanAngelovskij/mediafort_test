CREATE TABLE `subscription` (
  `id` int(11) NOT NULL auto_increment,
  `dt` int(11) NOT NULL default 0,
  `email` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
);

CREATE TABLE IF NOT EXISTS `catalog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(250) NOT NULL,
  `logo` varchar(250) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `mail` varchar(50) NOT NULL,
  `createdAt` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=7 ;

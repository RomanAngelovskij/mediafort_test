{foreach from=$sites item=site}
    <div class="site">
        <img src="{$site.logo}">
        <a href="/catalog/view/{$site.id}">{$site.name}</a>
        <div class="description">
            {$site.description|truncate:100}
        </div>
    </div>
{/foreach}